//Begin: 20210909
const GAME_STATE = {
  FirstCardAwaits: 'FirstCardAwaits',
  SecondCardAwaits: 'SecondCardAwaits',
  CardsMatchFailed: 'CardsMatchFailed',
  CardsMatched: 'CardsMatched',
  GameFinished: 'GameFinished'
}
//End: 20210909

const Symbols = [
  'https://image.flaticon.com/icons/svg/105/105223.svg', // 黑桃
  'https://image.flaticon.com/icons/svg/105/105220.svg', // 愛心
  'https://image.flaticon.com/icons/svg/105/105212.svg', // 方塊
  'https://image.flaticon.com/icons/svg/105/105219.svg' // 梅花
]

//********* VIEW *********
const view = {
  getCardElement(index) {
    return `<div data-index="${index}" class="card back"></div>`
    // html可以用data-index來傳參數
  },

  getCardContent(index) {
    const number = this.transformNumber((index % 13) + 1)
    const symbol = Symbols[Math.floor(index / 13)]
    return `
      <p>${number}</p>
      <img src="${symbol}" />
      <p>${number}</p>
    `
  },

  transformNumber(number) {
    switch (number) {
      case 1:
        return 'A'
      case 11:
        return 'J'
      case 12:
        return 'Q'
      case 13:
        return 'K'
      default:
        return number
    }
  },

  displayCards(indexes) {
    const rootElement = document.querySelector('#cards')
    // rootElement.innerHTML = this.getCardElement(10) //生成一張的
    // rootElement.innerHTML = Array.from(Array(52).keys()).map(index => this.getCardElement(index)).join('') //從頭排序的
    // rootElement.innerHTML = utility.getRandomNumberArray(52).map(index => this.getCardElement(index)).join('') //自己call utility
    rootElement.innerHTML = indexes.map(index => this.getCardElement(index)).join('') //Begin:20210909 //END
  },
  //Begin: 20210910 refactor
  flipCards(...cards) {
    cards.map(card => {
      if (card.classList.contains('back')) { //正面
        card.classList.remove('back')
        card.innerHTML = this.getCardContent(Number(card.dataset.index))
        return
      }
      card.classList.add('back')//背面
      card.innerHTML = null
    })
  },
  //End: 20210910 refactor
  //Begin: 20210909 20210910
  //如果是配對的,class加上paired
  pairCards(...cards) {
    cards.map(card => {
      card.classList.add('paired')
    })
  },
  renderScore(score) {
    document.querySelector(".score").innerHTML = `Score: ${score}`;
  },
  renderContinue(continue_correct) {
    document.querySelector(".continue_correct").innerHTML = `Contine correct:  ${continue_correct}`;
  },
  renderTriedTimes(times) {
    document.querySelector(".tried").innerHTML = `You've tried: ${times} times`;
  },
  renderLeft(cards_left) {
    document.querySelector(".left").innerHTML = `${cards_left} cards left`;
  },
  //End: 20210909 20210910
  // Begin:20210916 
  appendWrongAnimation(...cards) {
    cards.map(card => {
      card.classList.add('wrong')
      card.addEventListener('animationend', event => event.target.classList.remove('wrong'), { once: true })
    })
  },
  showGameFinished() {
    const div = document.createElement('div')
    div.classList.add('completed')
    div.innerHTML = `
      <p>Complete!</p>
      <p>Score: ${model.score}</p>
      <p>You've tried: ${model.triedTimes} times</p>
    `
    const header = document.querySelector('#header')
    header.before(div)
  }
  // End:20210916
}

//***** Model *****/
//Begin: 20210909
const model = {
  revealedCards: [],
  continue_correct: 0,
  triedTimes: 0,
  score: 0,
  cards_left: 52,
  GAMEOVERCARDNUM: 40,
  //判斷翻開的兩張是不是一樣的數字
  isRevealedCardsMatched() {
    return this.revealedCards[0].dataset.index % 13 === this.revealedCards[1].dataset.index % 13
  }

}
//End: 20210909

//****** Controler ******/
//Begin: 20210909
const controller = {
  currentState: GAME_STATE.FirstCardAwaits,
  generateCards() {
    view.displayCards(utility.getRandomNumberArray(52))
  },

  dispatchCardAction(card) {
    if (!card.classList.contains('back')) {
      return
    }
    switch (this.currentState) {
      case GAME_STATE.FirstCardAwaits:
        view.flipCards(card)//20210910
        model.revealedCards.push(card)
        this.currentState = GAME_STATE.SecondCardAwaits
        break
      case GAME_STATE.SecondCardAwaits:
        view.renderTriedTimes(++model.triedTimes) //Begin:20210910 //End
        view.flipCards(card)//20210910
        model.revealedCards.push(card)
        if (model.isRevealedCardsMatched()) { // 配對成功
          this.currentState = GAME_STATE.CardsMatched
          view.pairCards(...model.revealedCards)//Begin:20210910 //End
          model.revealedCards = [] //清空翻的牌
          model.cards_left -= 2
          model.score += 10 * (1 + model.continue_correct)
          model.continue_correct += 1
          console.log('Score', model.score, ' ,continue correct ', model.continue_correct, ' times, ', model.cards_left, ' cards left.')
          // Begin:20210910 渲染分數
          view.renderScore(model.score)
          view.renderContinue(model.continue_correct)
          view.renderLeft(model.cards_left)
          //End:20210910

          //Begin: 20210916 遊戲結束
          if (model.cards_left <= model.GAMEOVERCARDNUM) {
            console.log('showGameFinished')
            this.currentState = GAME_STATE.GameFinished
            view.showGameFinished()
            return
          }
          //End: 20210916 遊戲結束
          this.currentState = GAME_STATE.FirstCardAwaits
        } else { //配對失敗
          this.currentState = GAME_STATE.CardsMatchFailed
          view.appendWrongAnimation(...model.revealedCards) //Begin:20210916 //End
          //Begin: 20210910
          setTimeout(this.resetCards, 1000)
          //End: 20210910
        }
        break
    }
    console.log('this.currentState', this.currentState)
    console.log('revealedCards', model.revealedCards.map(card => card.dataset.index))
  },

  //Begin: 20210910
  resetCards() {
    view.flipCards(...model.revealedCards)
    model.revealedCards = []
    model.continue_correct = 0
    controller.currentState = GAME_STATE.FirstCardAwaits
  }
  //End:20210910

}
//End: 20210909

//********* UTILITY *********
const utility = {
  // Fisher-Yates Shuffle algorithm
  getRandomNumberArray(count) {
    const number = Array.from(Array(count).keys())
    for (let index = number.length - 1; index > 0; index--) {
      let randomIndex = Math.floor(Math.random() * (index + 1))
        ;[number[index], number[randomIndex]] = [number[randomIndex], number[index]]
    }
    return number
  }
}

//Node List(array-like)
controller.generateCards() //Begin: 20210909 //End
document.querySelectorAll('.card').forEach(card => {
  card.addEventListener('click', event => {
    // view.flipCard(card)
    controller.dispatchCardAction(card) //Begin: 20210909 //End
  })
})